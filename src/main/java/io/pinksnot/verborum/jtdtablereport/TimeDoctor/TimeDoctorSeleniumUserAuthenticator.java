package io.pinksnot.verborum.jtdtablereport.TimeDoctor;

import io.pinksnot.verborum.jtdtablereport.TimeDoctor.DTO.TimeDoctorUser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

// TODO: Kill me plz
public class TimeDoctorSeleniumUserAuthenticator {
    private final FirefoxDriver driver;

    private final String url = "https://webapi.timedoctor.com/oauth/v2/auth?client_id=2807_4dz0hh694e80wwo8c044wc0o0g4s0osww04kw44wwc0ccg48gs&redirect_uri=https://codeberg.org/verborum&response_type=token"; // Lol bigger than yours yea?

    public TimeDoctorSeleniumUserAuthenticator() {
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(true);

        this.driver = new FirefoxDriver(options);
    }

    public void authUser(TimeDoctorUser user)
            throws MalformedURLException {
        this.driver.get(url);

        WebElement webElementLogin = this.driver.findElement(
                By.id("username")
        );

        WebElement webElementPassword = this.driver.findElement(
                By.id("password")
        );

        WebElement webElementFirstButton = this.driver.findElement(
                By.className("button-field")
        );

        webElementLogin.sendKeys(user.getEmail());
        webElementPassword.sendKeys(user.getPassword());

        webElementFirstButton.click();

        WebElement webElementSecondButton = this.driver.findElement(
                By.className("button-field")
        );

        webElementSecondButton.click();

        String urlWithTokens = this.driver.getCurrentUrl();

        fillUserTokens(user, urlWithTokens);

        this.driver.quit();
    }

    private void fillUserTokens(TimeDoctorUser user, String urlWithTokens)
            throws MalformedURLException {
        Map<String, String> queryPairs = this.splitQueryFromUrl(urlWithTokens);

        user
                .setAccessToken(queryPairs.get("access_token"))
                .setRefreshToken(queryPairs.get("refresh_token"))
        ;
    }

    private Map<String, String> splitQueryFromUrl(String strUrl)
            throws MalformedURLException {
        URL url = new URL(strUrl);

        Map<String, String> queryPairs = new LinkedHashMap<>();

        String query = url.getRef();
        String[] pairs = query.split("&");

        for (String pair : pairs) {
            int idx = pair.indexOf("=");

            queryPairs.put(
                    URLDecoder.decode(
                            pair.substring(0, idx),
                            StandardCharsets.UTF_8
                    ),
                    URLDecoder.decode(
                            pair.substring(idx + 1),
                            StandardCharsets.UTF_8
                    )
            );
        }

        return queryPairs;
    }
}
