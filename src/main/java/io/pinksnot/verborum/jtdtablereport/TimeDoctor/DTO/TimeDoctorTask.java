package io.pinksnot.verborum.jtdtablereport.TimeDoctor.DTO;

public class TimeDoctorTask {
    private final String name;

    private final String project;

    private final double length;

    public TimeDoctorTask(String name, String project, double length) {
        this.name    = name;
        this.project = project;
        this.length  = length;
    }

    public String getName() {
        return this.name;
    }

    public String getProject() {
        return this.project;
    }

    public double getLength() {
        return this.length;
    }
}
