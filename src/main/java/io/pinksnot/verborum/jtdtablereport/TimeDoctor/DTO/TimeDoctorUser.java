package io.pinksnot.verborum.jtdtablereport.TimeDoctor.DTO;

import java.util.Base64;

public class TimeDoctorUser {
    private final String email;

    private final String password;

    private String accessToken;

    private String refreshToken;

    public TimeDoctorUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public TimeDoctorUser setAccessToken(String accessToken) {
        this.accessToken = accessToken;

        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public TimeDoctorUser setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;

        return this;
    }
}
