package io.pinksnot.verborum.jtdtablereport.TimeDoctor;

import io.pinksnot.verborum.jtdtablereport.Jira.Exception.JiraConnectionException;
import io.pinksnot.verborum.jtdtablereport.TimeDoctor.DTO.TimeDoctorTask;
import io.pinksnot.verborum.jtdtablereport.TimeDoctor.DTO.TimeDoctorUser;
import io.pinksnot.verborum.jtdtablereport.TimeDoctor.Exception.TimeDoctorConnectionException;
import okhttp3.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class TimeDoctorClient {
    private final OkHttpClient client;

    private final String baseUrl;

    public TimeDoctorClient() {
        this.client = new OkHttpClient()
                .newBuilder()
                .build()
        ;

        this.baseUrl = "https://webapi.timedoctor.com/v1.1/companies/630277/%s";
    }

    public ArrayList<TimeDoctorTask> getTasksArrayList(TimeDoctorUser user)
            throws IOException,
                   TimeDoctorConnectionException {
        ArrayList<TimeDoctorTask> tasksArrayList = new ArrayList<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd");
        LocalDate localDate   = LocalDate.now();
        String currentDateStr = dtf.format(localDate);

        String bodyStr = "{\"start_date\": \"%s\",\"end_date\": \"%s\"}"
                .formatted(currentDateStr, currentDateStr)
                ;

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body    = RequestBody.create(mediaType, bodyStr);

        String url = this.baseUrl.formatted("worklogs");

        Request request = new Request.Builder()
                .url(url)
                .method("GET", body)
                .addHeader("Authorization", "Basic %s".formatted(user.getAccessToken()))
                .build()
                ;

        Response response = this.client.newCall(request).execute();

        String json = this.getStringFromResponse(response);

        // TODO: Parse json here and add tasks to tasksArrayList var

        return tasksArrayList;
    }

    private String getStringFromResponse(Response response)
            throws IOException,
                   TimeDoctorConnectionException {
        if (response.body() != null) {
            return response.body().string();
        } else {
            throw new TimeDoctorConnectionException(
                    "Code: %d".formatted(response.code())
            );
        }
    }
}
