package io.pinksnot.verborum.jtdtablereport.TimeDoctor.Exception;

public class TimeDoctorConnectionException extends Exception {
    public TimeDoctorConnectionException(String errorMessage) {
        super(errorMessage);
    }
}
