package io.pinksnot.verborum.jtdtablereport.Jira.DTO;

import org.json.JSONArray;
import org.json.JSONObject;

public class JiraError {
    private final String[] errorMessages;

    private final int code;

    public JiraError(String errorMessageJsonString, int code) {
        JSONObject jsonObject = new JSONObject(errorMessageJsonString);

        JSONArray jsonArray = (JSONArray) jsonObject.get("errorMessages");

        this.errorMessages = jsonArray.toList().toArray(new String[0]);
        this.code          = code;
    }

    public String[] getErrorMessages() {
        return errorMessages;
    }

    public int getCode() {
        return code;
    }
}
