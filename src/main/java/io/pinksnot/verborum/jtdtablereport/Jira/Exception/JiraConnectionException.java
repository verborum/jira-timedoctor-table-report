package io.pinksnot.verborum.jtdtablereport.Jira.Exception;

public class JiraConnectionException extends Exception {
    public JiraConnectionException(String errorMessage) {
        super(errorMessage);
    }
}
