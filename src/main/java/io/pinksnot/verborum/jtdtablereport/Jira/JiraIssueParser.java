package io.pinksnot.verborum.jtdtablereport.Jira;

import io.pinksnot.verborum.jtdtablereport.Jira.DTO.JiraIssue;
import org.json.JSONObject;

public class JiraIssueParser {
    public static JiraIssue createIssueFromJson(String jsonString) {
        JiraIssue jiraIssue = new JiraIssue();

        JSONObject jsonObject = new JSONObject(jsonString);

        String summary = jsonObject.getJSONObject("fields").getString("summary");
        String key     = jsonObject.getString("key");

        String originalEstimate = jsonObject
                .getJSONObject("fields")
                .getJSONObject("timetracking")
                .getString("originalEstimate")
        ;

        jiraIssue
                .setKey(key)
                .setSummary(summary)
                .setOriginalEstimate(originalEstimate)
        ;

        if (jsonObject.getJSONObject("fields").has("parent")) {
            initIssueParentBaseInfo(jiraIssue, jsonObject);
        }

        return jiraIssue;
    }

    private static void initIssueParentBaseInfo(JiraIssue jiraIssue, JSONObject jsonObject) {

            JSONObject parent = jsonObject.getJSONObject("fields").getJSONObject("parent");

            String parentKey     = parent.getString("key");
            String parentSummary = parent.getJSONObject("fields").getString("summary");

            jiraIssue
                    .setParentKey(parentKey)
                    .setParentSummary(parentSummary)
            ;
    }
}
