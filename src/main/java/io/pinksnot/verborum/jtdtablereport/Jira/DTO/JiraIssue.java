package io.pinksnot.verborum.jtdtablereport.Jira.DTO;

public class JiraIssue {
    private String key;

    private String summary;

    private String originalEstimate;

    private String parentKey;

    private String parentSummary;

    private String url;

    @Override
    public String toString() {
        if (parentSummary != null) {
            return "%s - %s > %s".formatted(this.key, this.parentSummary, this.summary);
        }

        return "%s - %s".formatted(this.key, this.summary);
    }

    public String getKey() {
        return key;
    }

    public JiraIssue setKey(String key) {
        this.key = key;

        return this;
    }

    public String getSummary() {
        return summary;
    }

    public JiraIssue setSummary(String summary) {
        this.summary = summary;

        return this;
    }

    public String getOriginalEstimate() {
        return originalEstimate;
    }

    public JiraIssue setOriginalEstimate(String originalEstimate) {
        this.originalEstimate = originalEstimate;

        return this;
    }

    public String getParentKey() {
        return parentKey;
    }

    public JiraIssue setParentKey(String parentKey) {
        this.parentKey = parentKey;

        return this;
    }

    public String getParentSummary() {
        return parentSummary;
    }

    public JiraIssue setParentSummary(String parentSummary) {
        this.parentSummary = parentSummary;

        return this;
    }

    public String getUrl() {
        return url;
    }

    public JiraIssue setUrl(String url) {
        this.url = url;

        return this;
    }
}
