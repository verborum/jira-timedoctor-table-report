package io.pinksnot.verborum.jtdtablereport.Jira.DTO;

import java.util.Base64;

public class JiraUser {
    private final String auth;

    public JiraUser(String username, String token) {
        this.auth = "%s:%s".formatted(username, token);
    }

    public String getBasicToken() {
        return Base64.getEncoder().encodeToString(this.auth.getBytes());
    }
}
