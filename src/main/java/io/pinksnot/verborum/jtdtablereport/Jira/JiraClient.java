package io.pinksnot.verborum.jtdtablereport.Jira;

import io.pinksnot.verborum.jtdtablereport.Jira.DTO.JiraError;
import io.pinksnot.verborum.jtdtablereport.Jira.DTO.JiraIssue;
import io.pinksnot.verborum.jtdtablereport.Jira.DTO.JiraUser;
import io.pinksnot.verborum.jtdtablereport.Jira.Exception.JiraConnectionException;
import io.pinksnot.verborum.jtdtablereport.Jira.Exception.JiraErrorException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class JiraClient {
    private final OkHttpClient client;

    final private String baseUrl;

    public JiraClient(String host) {
        this.client = new OkHttpClient()
                .newBuilder()
                .build()
        ;

        this.baseUrl = "https://%s.atlassian.net".formatted(host);
    }

    public JiraIssue getIssueByKey(JiraUser user, String issueKey) throws IOException, JiraConnectionException, JiraErrorException {
        String issueRoute = "/rest/api/3/issue/%s";

        String issueFullUrl = "%s".formatted(this.baseUrl) + issueRoute.formatted(issueKey);

        Request request = new Request.Builder()
                .url(issueFullUrl)
                .method("GET", null)
                .addHeader("Authorization", "Basic %s".formatted(user.getBasicToken()))
                .build()
        ;

        Response response = this.client.newCall(request).execute();

        String json = this.getStringFromResponse(issueKey, response);

        this.checkSuccess(response, json);

        JiraIssue issue = JiraIssueParser.createIssueFromJson(json);

        issue.setUrl("%s/browse/%s".formatted(this.baseUrl, issueKey));

        return issue;
    }

    private String getStringFromResponse(String issueKey, Response response) throws IOException, JiraConnectionException {
        if (response.body() != null) {
            return response.body().string();
        } else {
            throw new JiraConnectionException(
                    "Empty response's body with issue key %s. Code: %d"
                            .formatted(issueKey, response.code())
            );
        }
    }

    private void checkSuccess(Response response, String json) throws JiraErrorException {
        if (!response.isSuccessful()) {
            JiraError jiraError = new JiraError(json, response.code());

            throw new JiraErrorException(jiraError);
        }
    }
}
