package io.pinksnot.verborum.jtdtablereport.Jira.Exception;

import io.pinksnot.verborum.jtdtablereport.Jira.DTO.JiraError;

public class JiraErrorException extends Exception {
    private final JiraError jiraError;

    public JiraErrorException(JiraError jiraError) {
        super(
                "%d %s"
                        .formatted(jiraError.getCode(), jiraError.getErrorMessages()[0])
        );

        this.jiraError = jiraError;
    }

    public JiraError getJiraError() {
        return jiraError;
    }
}
